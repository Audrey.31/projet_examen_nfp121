import java.io.File;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.util.AbstractMap.SimpleImmutableEntry;

public class GestionFichierXML {

	public GestionFichierXML() {}
	
	/** Extrait les donn�es abscisse, ordonn�e et valeur d'un fichier XML et les stocke dans un Iterable
	 * @param nomFichier nom du fichier XML � partir duquel r�cup�rer les donn�es
	 */
	public static Iterable<SimpleImmutableEntry<Position, Double>> traiter(String nomFichier) {
		List<SimpleImmutableEntry<Position, Double>> donnees = new ArrayList<>();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document= builder.parse(new File("src/" + nomFichier + ".xml"));
			Element racine = document.getDocumentElement();
			NodeList noeudsDonnee = racine.getChildNodes();
			
			for (int i = 0; i < noeudsDonnee.getLength(); i++) {
				if (noeudsDonnee.item(i).getNodeType() == Node.ELEMENT_NODE) {
					Element donnee = (Element) noeudsDonnee.item(i);
					double valeur = Double.parseDouble(donnee.getAttribute("valeur"));
					Element nodeX = (Element) donnee.getElementsByTagName("x").item(0);
					int x = Integer.parseInt(nodeX.getTextContent());
					Element nodeY = (Element) donnee.getElementsByTagName("y").item(0);
					int y = Integer.parseInt(nodeY.getTextContent());
					Position position = new Position(x, y);
					donnees.add(new AbstractMap.SimpleImmutableEntry<Position, Double>(position, valeur));
				}
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return donnees;
	}
}
