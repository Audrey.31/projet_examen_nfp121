import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Map;

import org.jdom2.DocType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 * GenerateurXML écrit dans un fichier, à charque fin de lot, toutes
 * les données lues en indiquant le lot dans le fichier XML.
 *
 * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class GenerateurXML extends Traitement {
	
	private String nomFichier;
	private Donnees donnees;
	
	public GenerateurXML(String nomFichier) {
		this.nomFichier = nomFichier;
	}
	
	@Override
	protected void gererDebutLotLocal(String nomLot) {
		this.donnees = new Donnees();
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		super.traiter(position, valeur);
		this.donnees.traiter(position, valeur);
	}

	@Override
	public void gererFinLotLocal(String nomLot) {
		Element lot = new Element("lot");
		Element donnees = new Element("donnees");
		lot.setAttribute("nom", nomLot);
		lot.addContent(donnees);
		
		// Boucle sur les donn�es et cr�� les �l�ments XML
		ArrayList<Map<Position, Double>> donneesMap = this.donnees.donnees();
		for (Map<Position, Double> map : donneesMap) {
			for (Map.Entry<Position, Double> entry : map.entrySet()) {
				Element donnee = new Element("donnee");
				donnee.setAttribute("id", entry.getKey().toString());
				donnee.setAttribute("valeur", entry.getValue().toString());
				donnees.addContent(donnee);
				Element x = new Element("x");
				Element y = new Element("y");
				x.setText(Integer.toString(entry.getKey().x));
				y.setText(Integer.toString(entry.getKey().y));
				donnee.addContent(x);
				donnee.addContent(y);
			}
		}
		
		/* Teste si un fichier XML avec le nom pass� en param�tre du constructeur existe;
		 * Si c'est le cas �criture des �l�ments XML dans le fichier existant;
		 * Sinon cr�ation d'un nouveau fichier et �criture des �l�ments XML
		 */
        try {
           XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
           File fichier = new File("src/" + this.nomFichier + ".xml");
           if (fichier.exists()) {
        	   FileWriter fw = new FileWriter(fichier, true);
        	   fw.write(sortie.outputString(lot));
        	   fw.flush();
               fw.close();
           }
           else {
        	   Document document = new Document(lot, new DocType(this.nomFichier + ".xml", "generateur.dtd"));
        	   sortie.output(document, new FileOutputStream("src/" + this.nomFichier + ".xml"));
           }
        }
        catch (java.io.IOException e){
        	System.out.println(e.getMessage());
        }
	}
}
