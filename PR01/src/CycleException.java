public class CycleException extends RuntimeException {
	
	public CycleException() {
		super();
	}
	
	public CycleException(String message) {
		super(message);
	}
}
