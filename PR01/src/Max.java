/**
  * Max calcule le max des valeurs vues, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class Max extends Traitement {

	private double max = 0;
	
	public Max() {}
	
	@Override
	public void traiter(Position position, double valeur) {
		super.traiter(position, valeur);
		if (valeur > this.max) {
			this.max = valeur;
		}
	}
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println("Max " + nomLot + " : " + this.max);
	}
	
	public double max() {
		return this.max;
	}
}
