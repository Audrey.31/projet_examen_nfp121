import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;
import java.util.List;

public class GestionFichierTexte {
	
	public GestionFichierTexte() {}

	/* Extrait les donn�es abscisse, ordonn�e et valeur d'un fichier texte et les stocke dans un Iterable
	 * @param nomFichier nom du fichier texte � partir duquel r�cup�rer les donn�es
	 * @param typeFichier type du fichier (1 pour txt type 1 et 2 pour txt type 2)
	 */
	public static Iterable<SimpleImmutableEntry<Position, Double>> traiter(String nomFichier, int typeFichier) {
		List<SimpleImmutableEntry<Position, Double>> donnees = new ArrayList<>();
		try (BufferedReader in = new BufferedReader(new FileReader("src/" + nomFichier +".txt"))) {
			String ligne = null;
			while ((ligne = in.readLine()) != null) {
				String[] mots = ligne.split("\\s+");
				assert mots.length == 4;
				int x = 0, y = 0;
				double valeur = 0;
				if (typeFichier == 1) {
					valeur = Double.parseDouble(mots[3]);
					x = Integer.parseInt(mots[0]);
					y = Integer.parseInt(mots[1]);
				}
				else if (typeFichier == 2) {
					valeur = Double.parseDouble(mots[4]);
					x = Integer.parseInt(mots[1]);
					y = Integer.parseInt(mots[2]);
				}
				
				Position position = new Position(x, y);
				donnees.add(new AbstractMap.SimpleImmutableEntry<Position, Double>(position, valeur));
			}
			return donnees;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
