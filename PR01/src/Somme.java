/**
  * Somme calcule la sommee des valeurs, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class Somme extends SommeAbstrait {

	private double somme;
	
	public Somme() {}

	@Override
	public void traiter(Position position, double valeur) {
		super.traiter(position, valeur);
		somme += valeur;
	}
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + ": somme = " + this.somme());
	}

	@Override
	public double somme() {
		return somme;
	}
}
