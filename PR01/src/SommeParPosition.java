import java.util.*;

/**
  * SommeParPosition 
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class SommeParPosition extends Traitement {

	Map<Position, Double> sommeParPosition = new HashMap<>();
	
	public SommeParPosition() {}
	
	@Override
	public void traiter(Position position, double valeur) {
		super.traiter(position, valeur);
		boolean flag = false;
		for (Map.Entry<Position, Double> entry : sommeParPosition.entrySet()) {
			if (entry.getKey().equals(position)) {
				entry.setValue(entry.getValue() + valeur);
				flag = true;
			}
		}
		
		if (flag == false) {
			sommeParPosition.put(position, valeur);
		}
	}
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println("SommeParPosition " + nomLot + " :");
		for (Map.Entry<Position, Double> entry : sommeParPosition.entrySet()) {
			System.out.println(entry.getKey() + " -> " + entry.getValue());
		}
		System.out.println("Fin SommeParPosition");
	}
}
