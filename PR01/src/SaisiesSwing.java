import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;


public class SaisiesSwing extends JFrame implements ActionListener {
	
	private String nomFichier;
	private List<String> donnees = new ArrayList<String>();
	private JTextField abscisseField = new JTextField(10);
	private JTextField ordonneeField = new JTextField(10);
	private JTextField valeurField = new JTextField(10);
	private JButton validerBtn = new JButton("Valider");
	private JButton effacerBtn = new JButton("Effacer");
	private JButton terminerBtn = new JButton("Terminer");
	private boolean champsOk = true;
	private int nbDonnees = 0;
	
	public SaisiesSwing(String nomFichier) {
		// Cr�ation des �l�ments qui composent l'interface graphique
		super("Saisie donn�es");
		this.nomFichier = nomFichier;
		JPanel mainPanel = new JPanel();
		JPanel panelFields = new JPanel();
		JPanel panelBtn = new JPanel();
		
		JLabel abscisseText = new JLabel("Abscisse");
		abscisseText.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel ordonneeText = new JLabel("Ordonn�e");
		ordonneeText.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel valeurText = new JLabel("Valeur");
		valeurText.setHorizontalAlignment(SwingConstants.CENTER);
		
		panelFields.setLayout(new GridLayout(2, 3));
		panelFields.add(abscisseText);
		panelFields.add(ordonneeText);
		panelFields.add(valeurText);
		panelFields.add(abscisseField);
		panelFields.add(ordonneeField);
		panelFields.add(valeurField);
		
		panelBtn.setLayout(new FlowLayout());
		panelBtn.add(validerBtn);
		panelBtn.add(effacerBtn);
		panelBtn.add(terminerBtn);
		
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		mainPanel.add(panelFields);
		mainPanel.add(panelBtn);
		
		// Ajout d'un �couteur sur les boutons
		validerBtn.addActionListener(this);
		effacerBtn.addActionListener(this);
		terminerBtn.addActionListener(this);

		this.getContentPane().add(mainPanel);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();			
		this.setVisible(true);
	}

	// Teste quel bouton a �t� cliqu� et appelle la m�thode correspondante qui va g�rer l'�v�nement
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() ==  validerBtn) {
			traiterValider();
		}
		else if (e.getSource() ==  effacerBtn) {
			viderChamps();
		}
		else if (e.getSource() ==  terminerBtn) {
			traiterTerminer();
		}		
	}
	
	/* Si la v�rification des champs est ok les valeurs de ces champs sont ajout�es � la List donn�es
	 * L'attribut nbDonnees permet de d�terminer l'identifiant
	 */
	public void traiterValider() {
		champsOk = true;
		verifierChamps(abscisseField, false);
		verifierChamps(ordonneeField, false);
		verifierChamps(valeurField, true);

		if (champsOk == true) {
			nbDonnees++;
			donnees.add(abscisseField.getText());
			donnees.add(ordonneeField.getText());
			donnees.add(Integer.toString(nbDonnees));
			donnees.add(Double.toString(Double.parseDouble(valeurField.getText())));
			viderChamps();
		}
	}
	

	/* Ajoute un fond rouge au(x) champs vides ou ceux dont la valeur ne correspond pas au type attendu
	 * @param champs le champ � v�rifier
	 * @param champsDouble booleen qui d�termine si la valeur attendue est un int ou un double
	 */
	public void verifierChamps(JTextField champs, boolean champsDouble) {
		if (champs.getText().isEmpty()) {
			champs.setBackground(Color.RED);
			champsOk = false;
			return;
		}
		
		try {
			if (champsDouble = true) {
				Double.parseDouble(champs.getText());
			}
			else {
				Integer.parseInt(champs.getText());
			}
		}
		catch(NumberFormatException e) {
			champs.setBackground(Color.RED);
			champsOk = false;
		}
	}
	
	// Cr�� un fichier et boucle sur la List donn�es pour y �crire les donn�es (4 �l�ments par ligne : abscisse, ordonn�e, id, valeur)
	public void traiterTerminer() {
		File fichier = new File(this.nomFichier);
		try {
			FileWriter fw = new FileWriter(fichier);
			String str = "";
			for (int i = 0, j = 1; i < donnees.size(); i++, j++) {
				str += donnees.get(i) + " ";
				if (j % 4 == 0) {
					str += "\n";
					fw.write(str);
					str = "";
				}
			}
     	   	
     	   fw.flush();
     	   fw.close();
     	   this.dispose(); 
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	// Vide les champs et remet les fonds blanc
	public void viderChamps() {
		abscisseField.setText("");
		ordonneeField.setText("");
		valeurField.setText("");
		abscisseField.setBackground(Color.WHITE);
		ordonneeField.setBackground(Color.WHITE);
		valeurField.setBackground(Color.WHITE);
	}
	
	
	
	public static void main(String[] args) {
		if (args.length > 0) {
			new SaisiesSwing(args[0]);
		}
		else {
			System.out.println("Veuillez saisir un nom de fichier en argument.");
		}
	}
}
	
