import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Max calcule le min des valeurs vues, quelque soit le lot.
 */

public class Min extends Traitement {
	
	private Donnees donnees;
	private double min;
	
	public Min() {}
	
	@Override
	protected void gererDebutLotLocal(String nomLot) {
		this.donnees = new Donnees();
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		super.traiter(position, valeur);
		this.donnees.traiter(position, valeur);
	}

	@Override
	public void gererFinLotLocal(String nomLot) {
		ArrayList<Map<Position, Double>> donneesMap = this.donnees.donnees();
		double minTemp = 0.0;
		int index = 0;
		
		for (Map<Position, Double> map : donneesMap) {
			for (Map.Entry<Position, Double> entry : map.entrySet()) {
				if (index == 0) {
					minTemp = entry.getValue();
				}
				else if (entry.getValue() < minTemp) {
					minTemp = entry.getValue();
				}
			}
			index++;
		}
		this.min = minTemp;
		System.out.println("Min " + nomLot + " : " + this.min);
	}
	
	public double min() {
		return this.min;
	}
}
