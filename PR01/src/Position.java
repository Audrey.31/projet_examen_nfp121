/** Définir une position.  */
public class Position {
	public int x;
	public int y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override public String toString() {
		return super.toString() + "(" + x + "," + y + ")";
	}
	
	@Override
	public boolean equals(Object obj) {
		Position p = (Position) obj;
		if (p.x == x && p.y == y) {
			return true;
		}
		return false;
	}
}
