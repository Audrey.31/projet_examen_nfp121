import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

/**
  * Donnees enregistre toutes les données reçues, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Donnees extends Traitement {
	ArrayList<Map<Position, Double>> donnees = new ArrayList<>();
	
	public Donnees() {}
	
	@Override
	public void traiter(Position position, double valeur) {
		super.traiter(position, valeur);
		Map<Position, Double> donnee = new HashMap<>();
		donnee.put(position, valeur);
		donnees.add(donnee);
	}
	
	public ArrayList<Map<Position, Double>> donnees() {
		return donnees;
	}	
}
