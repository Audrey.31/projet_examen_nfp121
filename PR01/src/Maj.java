import java.util.ArrayList;
import java.util.List;

/**
 * Maj indique pour chaque lot les positions mises à jour (ou ajoutées)
 * lors du traitement de ce lot.
 *
 * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class Maj extends Traitement {

	private List<Position> positions = new ArrayList<>();	
	
	public Maj() {}
	
	@Override
	public void traiter(Position position, double valeur) {
		super.traiter(position, valeur);
		this.positions.add(position);
	}
	
	public List<Position> positions() {
		return this.positions;
	}
}
