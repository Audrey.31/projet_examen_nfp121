import java.util.ArrayList;
import java.util.Map;

/**
  * Normaliseur normalise les données d'un lot en utilisant une transformation affine.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Normaliseur extends Traitement {
	
	private double debut;
	private double fin;
	private Max max;
	private Donnees donnees;
	
	public Normaliseur(double debut, double fin) {
		this.debut = debut;
		this.fin = fin;
	}
	
	@Override
	protected void gererDebutLotLocal(String nomLot) {
		this.max = new Max();
		this.donnees = new Donnees();
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		this.max.traiter(position, valeur);
		this.donnees.traiter(position, valeur);
	}
	
	// R�cup�re les donn�es er le max du lot puis boucle sur les donn�es pour les normaliser et les envoyer au traitement suivant
	@Override
		public void gererFinLotLocal(String nomLot) {
		ArrayList<Map<Position, Double>> donneesMap = this.donnees.donnees();
		double max = this.max.max();
		double min = max  * -1;
		for (Map<Position, Double> map : donneesMap) {
			for (Map.Entry<Position, Double> donnee : map.entrySet()) {
				double a = (max - min) / (this.fin - this.debut);
				double valeur = a * donnee.getValue() + (this.debut - a * min);
				super.traiter(donnee.getKey(), Math.round(valeur));
			}
		}
		
	}
}

