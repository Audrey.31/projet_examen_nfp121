import java.lang.reflect.*;
import java.util.*;

/**
  * TraitementBuilder 
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class TraitementBuilder {

	/** Retourne un objet de type Class correspondant au nom en parametre.
	 * Exemples :
	 *   - int donne int.class
	 *   - Normaliseur donne Normaliseur.class
	 */
	Class<?> analyserType(String nomType) throws ClassNotFoundException {
		Class<?> nomClasse = null;
		try {
			nomClasse = Class.forName(nomType);
		}
		catch(ClassNotFoundException e) {
			if (nomType.equals("int")) {
				nomClasse = int.class;
			}
			else if (nomType.equals("double")) {
				nomClasse = double.class;
			}
		}
		return nomClasse;
	}

	/** Cree l'objet java qui correspond au type formel en exploitant le � mot � suviant du scanner.
	 * Exemple : si formel est int.class, le mot suivant doit etre un entier et le resulat est l'entier correspondant.
	 * Ici, on peut se limiter aux types utlises dans le projet : int, double et String.
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	static Object decoderEffectif(Class<?> formel, Scanner in) 
			throws NoSuchMethodException, SecurityException, InstantiationException, 
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
//		if (in.hasNextInt()) {
//			Method methode = formel.getMethod("valueOf", int.class);
//			return methode.invoke(formel, in.nextInt());
//		}
//		else if (in.hasNextDouble()) {
//			Method methode = formel.getMethod("valueOf", double.class);
//			return methode.invoke(formel, in.nextDouble());
//		}
//		else {
//			Method methode = formel.getMethod("valueOf", String.class);
//			return methode.invoke(formel, in.next());
//		}
		
		if (in.hasNextInt()) {
			return Integer.valueOf(in.nextInt());
		}
		else {
			String str = in.next();
			try {
				double nb = Double.parseDouble(str);
				return nb;
			}
			catch(NumberFormatException e) {
				return String.valueOf(str);
			}
		}
	}


	/** Definition de la signature, les parametres formels, mais aussi les parametres effectifs.  */
	static class Signature {
		Class<?>[] formels;
		Object[] effectifs;

		public Signature(Class<?>[] formels, Object[] effectifs) {
			this.formels = formels;
			this.effectifs = effectifs;
		}
	}

	
	/** Analyser une signature pour retrouver les parametres formels et les parametres effectifs.
	 * Exemple � 3 double 0.0 String xyz int -5 � donne
	 *   - [double.class, String.class, int.class] pour les parametres effectifs et
	 *   - [0.0, "xyz", -5] pour les parametres formels.
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * 
	 * Boucle sur les param�tres re�us et les stocke dans deux tableaux (tableaux de Class pour les param�tres formels et tableau d'Object pour les effectifs)
	 * La classe Signature est ensuite instanci�e et retourn�e pour regrouper les param�tres
	 */
	Signature analyserSignature(Scanner in) 
			throws ClassNotFoundException, NoSuchMethodException, SecurityException,
			InstantiationException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		
		int nbParams = in.nextInt();
		Class<?>[] formels = new Class<?>[nbParams];
		Object[] effectifs = new Object[nbParams];
		 
		for (int i = 0; i < nbParams; i++) {
			Class<?> classeFormel = analyserType(in.next());
			formels[i] = classeFormel;
			effectifs[i] = decoderEffectif(classeFormel, new Scanner(in.next()));
		}
		TraitementBuilder.Signature signature = new TraitementBuilder.Signature(formels, effectifs);
		return signature;
	}


	/** Analyser la creation d'un objet.
	 * Exemple : � Normaliseur 2 double 0.0 double 100.0 � consiste a charger
	 * la classe Normaliseur, trouver le constructeur qui prend 2 double, et
	 * l'appeler en lui fournissant 0.0 et 100.0 comme parametres effectifs.
	 * 
	 * La variable str stocke les param�tres du traitement (nombre + param�tres formels et effectifs) et les envoie � la m�thode analyserSignature()
	 * pour r�cup�rer une instance de la classe Signature qui contient les param�tres formels sous forme d'un tableau de Class 
	 * et les param�tres effectif sous forme d'un tableau d'Object contenant les instances des diff�rents types primitifs
	 */
	Object analyserCreation(Scanner in)
		throws ClassNotFoundException, InvocationTargetException,
						  IllegalAccessException, NoSuchMethodException,
						  InstantiationException {
		
		Class<?> nomClasse = analyserType(in.next());
		String str = "";
		int nbParams = in.nextInt();
		str += Integer.toString(nbParams);
		for (int i = 1; i <= nbParams; i++) {
			str += " " + in.next();
			str += " " + in.next();
		}
		Signature signature = analyserSignature(new Scanner(str));
		Constructor<?> constructeur = nomClasse.getDeclaredConstructor(signature.formels);
		return constructeur.newInstance(signature.effectifs);
	}


	/** Analyser un traitement.
	 * Exemples :
	 *   - � Somme 0 0 �
	 *   - � SupprimerPlusGrand 1 double 99.99 0 �
	 *   - � Somme 0 1 Max 0 0 �
	 *   - � Somme 0 2 Max 0 0 SupprimerPlusGrand 1 double 20.0 0 �
	 *   - � Somme 0 2 Max 0 0 SupprimerPlusGrand 1 double 20.0 1 Positions 0 0 �
	 * @param in le scanner a utiliser
	 * @param env l'environnement ou enregistrer les nouveaux traitements
	 * 
	 * La variable str stocke un traitement sous forme d'une chaine de caract�res et l'envoie � la m�thode analyserCreation() 
	 * pour r�cup�rer une instance de la classe de traitement correspondante
	 * La r�cursivit� est utilis�e pour ajouter le ou les �ventuels traitements suivants � l'instance;
	 * Si plusieurs traitements, ils sont ajout�s � un tableau qui sera ensuite pass� en param�tre de la m�thode ajouterSuivant()
	 */
	Traitement analyserTraitement(Scanner in, Map<String, Traitement> env)
		throws ClassNotFoundException, InvocationTargetException,
						  IllegalAccessException, NoSuchMethodException,
						  InstantiationException {

		String str = in.next();
		int nbParams = in.nextInt();
		str += " " + Integer.toString(nbParams);
		if (nbParams > 0) {
			for (int i = 1; i <= nbParams; i++) {
				str += " " + in.next();
				str += " " + in.next();
			}
		}
		Object obj = analyserCreation(new Scanner(str));

		int nbSuivant = in.nextInt();
		if (nbSuivant == 1) {
			((Traitement) obj).ajouterSuivants(analyserTraitement(in, null));
		}
		else if (nbSuivant > 1) {
			Traitement[] traitements = new Traitement[nbSuivant];
			for (int i = 0; i < nbSuivant; i++) {
				traitements[i] = analyserTraitement(in, null);
			}
			((Traitement) obj).ajouterSuivants(traitements);
		}
		return (Traitement) obj;	
	}


	/** Analyser un traitement.
	 * @param in le scanner a utiliser
	 * @param env l'environnement ou enregistrer les nouveaux traitements
	 */
	public Traitement traitement(Scanner in, Map<String, Traitement> env) {
		try {
			return analyserTraitement(in, env);
		} catch (Exception e) {
			throw new RuntimeException("Erreur sur l'analyse du traitement, "
					+ "voir la cause ci-dessous", e);
		}
	}
}
